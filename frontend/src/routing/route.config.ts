export const TELEMETRY_EXERCISE_URL = '/telemetry';
export const TURN_TICKET_DISPENSER_EXERCISE_URL = '/turn-ticket-dispenser';
export const TIRE_PRESSURE_MONITORING_EXERCISE_URL = '/tire-pressure-monitoring';