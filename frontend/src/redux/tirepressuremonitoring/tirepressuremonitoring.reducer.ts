import { Action, DataAction } from "../store/action.model";
import { TirePressureMonitoringState } from "./tirepressuremonitoring.model";
import actionTypes from '../actionTypes';

const initialState: TirePressureMonitoringState = { 
    isTooHigh: false 
};

const tirePressureMonitoringReducer = (
    state: TirePressureMonitoringState = initialState,
    action: Action
): TirePressureMonitoringState => {
    switch(action.type) {
        case actionTypes.STORE_TIRE_PRESSURE:
            const data = (action as DataAction<boolean>).data;
            return { 
                ...state,
                isTooHigh: data 
            };
        default:
            return state;
    }
}

export default tirePressureMonitoringReducer;