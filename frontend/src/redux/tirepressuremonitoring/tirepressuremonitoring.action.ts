import actionTypes from "../actionTypes";

export function storeTirePressure(isTooHigh: boolean) {
    return { type: actionTypes.STORE_TIRE_PRESSURE, data: isTooHigh };
}