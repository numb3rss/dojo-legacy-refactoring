export type TirePressureMonitoringState = {
    isTooHigh: boolean
};