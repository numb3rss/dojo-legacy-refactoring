import actionTypes from "../actionTypes";

export function storeDiagnosticInfo(diagnosticInfo: string) {
    return { type: actionTypes.STORE_DIAGNOSTIC_INFO, data: diagnosticInfo };
}