export type TelemetryState = {
    diagnosticInfo: string | undefined
};