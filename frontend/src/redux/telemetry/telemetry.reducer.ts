import { Action, DataAction } from "../store/action.model";
import { TelemetryState } from "./telemetry.model";
import actionTypes from '../actionTypes';

const initialState: TelemetryState = { 
    diagnosticInfo: undefined 
};

const telemetryReducer = (
    state: TelemetryState = initialState,
    action: Action
): TelemetryState => {
    switch(action.type) {
        case actionTypes.STORE_DIAGNOSTIC_INFO:
            const data = (action as DataAction<string>).data;
            return { 
                ...state,
                diagnosticInfo: data 
            };
        default:
            return state;
    }
}

export default telemetryReducer;