import actionTypes from "../actionTypes";

export function storeTurnNumber(turnNumber: number) {
    return { type: actionTypes.STORE_TURN_NUMBER, data: turnNumber };
}