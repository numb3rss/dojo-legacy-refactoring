import { Action, DataAction } from "../store/action.model";
import { TurnTicketDispenserState } from "./turnticketdispenser.model";
import actionTypes from '../actionTypes';

const initialState: TurnTicketDispenserState = { 
    turnNumber: -1 
};

const turnTicketDispenserReducer = (
    state: TurnTicketDispenserState = initialState,
    action: Action
): TurnTicketDispenserState => {
    switch(action.type) {
        case actionTypes.STORE_TURN_NUMBER:
            const data = (action as DataAction<number>).data;
            return { 
                ...state,
                turnNumber: data 
            };
        default:
            return state;
    }
}

export default turnTicketDispenserReducer;