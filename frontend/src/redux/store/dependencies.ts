import { IAlarmFactory } from '../../tirepressuremonitoring/alarmFactory.interface';
import { ITelemetryDiagnosticControlsFactory } from '../../telemetry/telemetryDiagnosticControlsFactory.interface';
export interface Dependencies {
    alarmFactory: IAlarmFactory,
    telemetryDiagnosticControlsFactory: ITelemetryDiagnosticControlsFactory
};