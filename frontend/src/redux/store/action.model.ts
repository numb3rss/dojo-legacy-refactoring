import { Action as ActionRedux } from 'redux';

export interface Action extends ActionRedux {
    type: string
};

export interface DataAction<T> extends Action {
    data: T
}