import { TurnTicketDispenserState } from "../turnticketdispenser/turnticketdispenser.model";

export type RootState = {
    turnTicketDispenser: TurnTicketDispenserState
};