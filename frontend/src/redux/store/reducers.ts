import { combineReducers } from "redux";
import turnTicketDispenserReducer from '../../redux/turnticketdispenser/turnticketdispenser.reducer';
import tirePressureMonitoringReducer from '../../redux/tirepressuremonitoring/tirepressuremonitoring.reducer';
import telemetryReducer from '../../redux/telemetry/telemetry.reducer';

const appReducer = () => combineReducers({
    turnTicketDispenser: turnTicketDispenserReducer,
    tirePressureMonitoring: tirePressureMonitoringReducer,
    telemetry: telemetryReducer
});

export const createRootReducer = () => (state, action) => appReducer()(state, action);