import { AnyAction, applyMiddleware, createStore, Store } from "redux";
import { Dependencies } from './dependencies';
import { composeWithDevTools } from 'redux-devtools-extension/index';
import thunk, { ThunkAction, ThunkDispatch, ThunkMiddleware } from "redux-thunk";
import { RootState } from './store.model';
import { createRootReducer } from './reducers';

export const configureStore = (dependencies: Partial<Dependencies> | null) => {
    return createStore(
        createRootReducer(),
        composeWithDevTools(
            applyMiddleware(
                dependencies ?
                    (thunk.withExtraArgument(dependencies) as ThunkMiddleware<
                        RootState,
                        AnyAction,
                        Dependencies
                    >) :
                    thunk
            )
        )
    )
}

export type ReduxStore = Store<RootState> & {
    dispatch: ThunkDispatch<RootState, Dependencies, AnyAction>;
};
  
export type ThunkResult<R> = ThunkAction<R, RootState, Dependencies, AnyAction>;
  