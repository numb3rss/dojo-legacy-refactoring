import TurnTicket from "./turnTicket";
import TurnNumberSequence from './turnNumberSequence';
import { ThunkResult } from '../redux/store/reduxStoreConfiguration';
import { storeTurnNumber } from "../redux/turnticketdispenser/turnticketdispenser.action";

const retrieveTurnNumber = (): ThunkResult<void> => (dispatch, getState) => {
    const newTurnNumber = TurnNumberSequence.getNextTurnNumber();
    const newTurnTicket = new TurnTicket(newTurnNumber);
    const turnNumber = newTurnTicket.getTurnNumber();
    dispatch(storeTurnNumber(turnNumber));
}

export default retrieveTurnNumber;
