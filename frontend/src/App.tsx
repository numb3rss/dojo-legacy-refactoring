import React from 'react';
import HomePage from './components/HomePage';

function App() {
  return (
    <div>
      <header>
        <HomePage />
      </header>
    </div>
  );
}

export default App;
