import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { configureStore } from './redux/store/reduxStoreConfiguration';
import { Provider } from 'react-redux';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import TelemetryPage from './components/telemetryexercise/TelemetryPage';
import TurnTicketDispenserPage from './components/turnticketdispenserexercise/TurnTicketDispenserPage';
import TirePressureMonitoringPage from './components/tirepressurcemonitoringexercise/TirePressureExerciseMonitoring';
import { TELEMETRY_EXERCISE_URL, TIRE_PRESSURE_MONITORING_EXERCISE_URL, TURN_TICKET_DISPENSER_EXERCISE_URL } from './routing/route.config';
import { AlarmFactory } from './tirepressuremonitoring/alarmFactory';
import { TelemetryDiagnosticControlsFactory } from './telemetry/telemetryDiagnosticControlsFactory';
import MathFormula from './tirepressuremonitoring/math';
declare global {
  interface Window {
    Cypress: any,
    store: any
  }
}
const math = new MathFormula();
const alarmFactory = new AlarmFactory(math);
const telemetryDiagnosticControlsFactory = new TelemetryDiagnosticControlsFactory(math);

const store = configureStore({
  alarmFactory,
  telemetryDiagnosticControlsFactory
});

if(window.Cypress) {
  window.store = store;
}

ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path={TELEMETRY_EXERCISE_URL} element={<TelemetryPage />} />
          <Route path={TURN_TICKET_DISPENSER_EXERCISE_URL} element={<TurnTicketDispenserPage />} />
          <Route path={TIRE_PRESSURE_MONITORING_EXERCISE_URL} element={<TirePressureMonitoringPage />} />
          <Route path="/" element={<App />} />
        </Routes>
      </BrowserRouter>
    </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
