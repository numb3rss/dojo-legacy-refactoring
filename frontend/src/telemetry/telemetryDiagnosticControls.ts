import { ITelemetryClient } from './telemetryClient.interface';

export default class TelemetryDiagnosticControls {
	private diagnosticChannelConnectionString: string;

	private readonly telemetryClient: ITelemetryClient;
	private diagnosticInfo: string;

	constructor(telemetryClient: ITelemetryClient) {
		this.telemetryClient = telemetryClient;
		this.diagnosticChannelConnectionString = '*111#';
		this.diagnosticInfo = '';
	}

	public readDiagnosticInfo() {
		return this.diagnosticInfo;
	}

	public writeDiagnosticInfo(newValue: string) {
		this.diagnosticInfo = newValue;
	}

	public checkTransmission() {
		this.diagnosticInfo = '';

		this.telemetryClient.disconnect();

		let retryLeft = 3;
		while (this.telemetryClient.getOnlineStatus() === false && retryLeft > 0) {
			this.telemetryClient.connect(this.diagnosticChannelConnectionString);
			retryLeft -= 1;
		}

		if (this.telemetryClient.getOnlineStatus() === false) {
			throw new Error('Unable to connect');
		}

		this.telemetryClient.send(this.telemetryClient.diagnosticMessage());
		this.diagnosticInfo = this.telemetryClient.receive();
	}
}
