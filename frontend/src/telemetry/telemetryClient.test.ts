import TelemetryClient from './telemetryClient';
import IMath from '../tirepressuremonitoring/math.interface';

describe('Unit tests for telemetryClient', () => {
    let telemetryClient: TelemetryClient;
    const mockedMath: jest.Mocked<IMath> = {
        random: jest.fn()
    };

    beforeEach(() => {
        telemetryClient = new TelemetryClient(mockedMath);
    })

    test.each([
        ['404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040', 0],
        ['838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383', 0.5],
        ['127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127127', 1]
    ])('should return message %s when receiving and no message has been sent with random %s', 
        (expectedMessage, randomMocked) => {
        mockedMath.random.mockReturnValue(randomMocked);

        const message = telemetryClient.receive();

        expect(message).toEqual(expectedMessage);
    })

    it('should return diagnostic message when message has been sent with the specific value', () => {
        const diagnosticMessageResultExpected = 
        'LAST TX rate................ 100 MBPS\r\n'
        + 'HIGHEST TX rate............. 100 MBPS\r\n'
        + 'LAST RX rate................ 100 MBPS\r\n'
        + 'HIGHEST RX rate............. 100 MBPS\r\n'
        + 'BIT RATE.................... 100000000\r\n'
        + 'WORD LEN.................... 16\r\n'
        + 'WORD/FRAME.................. 511\r\n'
        + 'BITS/FRAME.................. 8192\r\n'
        + 'MODULATION TYPE............. PCM/FM\r\n'
        + 'TX Digital Los.............. 0.75\r\n'
        + 'RX Digital Los.............. 0.10\r\n'
        + 'BEP Test.................... -5\r\n'
        + 'Local Rtrn Count............ 00\r\n'
        + 'Remote Rtrn Count........... 00';
        
        telemetryClient.send('AT#UD');
        const message = telemetryClient.receive();

        expect(message).toEqual(diagnosticMessageResultExpected);
    })

    it('should return missing message parameter when sending message without parameter', () => {
        try {
            telemetryClient.send('');
        }
        catch(error){
            expect(error.message).toEqual('missing message parameter');
        }
    })

    it('should return missing message parameter when connecting message without parameter', () => {
        try {
            telemetryClient.connect('');
        }
        catch(error){
            expect(error.message).toEqual('missing telemetryServerConnectionString parameter');
        }
    })

    it('should have online status equal to success', () => {
        mockedMath.random.mockReturnValue(0);

        telemetryClient.connect('connectionString');
        const onlineStatus = telemetryClient.getOnlineStatus();

        expect(onlineStatus).toBeTruthy();
    })

    it('should have online status not equal to success', () => {
        mockedMath.random.mockReturnValue(1);

        telemetryClient.connect('connectionString');
        const onlineStatus = telemetryClient.getOnlineStatus();

        expect(onlineStatus).toBeFalsy();
    })

    it('should be disconnected after successful connection', () => {
        mockedMath.random.mockReturnValue(0);

        telemetryClient.connect('connectionString');
        let onlineStatus = telemetryClient.getOnlineStatus();

        expect(onlineStatus).toBeTruthy();
        telemetryClient.disconnect();
        onlineStatus = telemetryClient.getOnlineStatus();
        
        expect(onlineStatus).toBeFalsy();
    })
})