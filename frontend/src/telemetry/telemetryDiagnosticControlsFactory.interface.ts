import TelemetryDiagnosticControls from "./telemetryDiagnosticControls";

export interface ITelemetryDiagnosticControlsFactory {
    create(): TelemetryDiagnosticControls
}