import { ITelemetryClient } from "./telemetryClient.interface"
import TelemetryDiagnosticControls from './telemetryDiagnosticControls';

describe('Unit tests telemetryDiagnosticControls', () => {
    let telemetryDiagnosticControls: TelemetryDiagnosticControls;
    const telemetryClientMocked: jest.Mocked<ITelemetryClient> = {
        diagnosticMessage: jest.fn(),
        getOnlineStatus: jest.fn(),
        connect: jest.fn(),
        disconnect: jest.fn(),
        send: jest.fn(),
        receive: jest.fn()
    }

    beforeEach(() => {
        telemetryDiagnosticControls = new TelemetryDiagnosticControls(telemetryClientMocked);
    })

    it('should cannot connect when checking transmission after 3 attempts', () => {
        telemetryClientMocked.getOnlineStatus.mockReturnValue(false);

        try{
            telemetryDiagnosticControls.checkTransmission();
        }
        catch(error){
            expect(error.message).toEqual('Unable to connect');
        }

        expect(telemetryClientMocked.connect).toHaveBeenCalledWith('*111#');
        expect(telemetryClientMocked.connect).toHaveBeenCalledTimes(3);
    })

    it('should connect when checking transmission with diagnostic info', () => {
        const diagnosticMessage = 'diagnosticMessage';
        const diagnosticInfo = 'diagnosticInfo';
        telemetryClientMocked.getOnlineStatus.mockReturnValue(true);
        telemetryClientMocked.diagnosticMessage.mockReturnValue(diagnosticMessage);
        telemetryClientMocked.receive.mockReturnValue(diagnosticInfo);
        
        telemetryDiagnosticControls.checkTransmission();

        expect(telemetryClientMocked.send).toHaveBeenCalledWith(diagnosticMessage);
        expect(telemetryClientMocked.send).toHaveBeenCalledTimes(1);
        expect(telemetryDiagnosticControls.readDiagnosticInfo()).toEqual(diagnosticInfo);
    })

    it('should write diagnostic info', () => {
        const diagnosticInfo = 'diagnosticInfo';
        
        telemetryDiagnosticControls.writeDiagnosticInfo(diagnosticInfo);
        
        expect(telemetryDiagnosticControls.readDiagnosticInfo()).toEqual(diagnosticInfo);
    })
})