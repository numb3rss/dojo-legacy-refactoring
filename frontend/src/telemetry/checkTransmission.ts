import { ThunkResult } from '../redux/store/reduxStoreConfiguration';
import { storeDiagnosticInfo } from '../redux/telemetry/telemetry.action';

const checkTransmission = (): ThunkResult<void> => (dispatch, getState, dependencies) => {
    const {
        telemetryDiagnosticControlsFactory
    } = dependencies;

    const telemetryDiagnosticControls = telemetryDiagnosticControlsFactory.create();
    telemetryDiagnosticControls.checkTransmission();
    dispatch(storeDiagnosticInfo(telemetryDiagnosticControls.readDiagnosticInfo()));
}

export default checkTransmission;
