export interface ITelemetryClient {
    diagnosticMessage();
    getOnlineStatus();
    connect(telemetryServerConnectionString: string);
    disconnect();
    send(message: string);
    receive();
}