import { configureStore } from '../redux/store/reduxStoreConfiguration';
import IMath from '../tirepressuremonitoring/math.interface';
import checkTransmission from './checkTransmission';
import { TelemetryDiagnosticControlsFactory } from './telemetryDiagnosticControlsFactory';

describe("Acceptance tests for use case checkTransmission", () => {
    let store;
    let initialState;

    const mathMocked: jest.Mocked<IMath> = {
        random: jest.fn()
    }

    beforeEach(() => {
        const telemetryDiagnosticControlsFactory = new TelemetryDiagnosticControlsFactory(mathMocked);
        store = configureStore({
            telemetryDiagnosticControlsFactory
        });
        initialState = store.getState();
    });

    it('should return diagnostic info when checking transmission', async () => {
        const diagnosticInfo = 'LAST TX rate................ 100 MBPS\r\n'
        + 'HIGHEST TX rate............. 100 MBPS\r\n'
        + 'LAST RX rate................ 100 MBPS\r\n'
        + 'HIGHEST RX rate............. 100 MBPS\r\n'
        + 'BIT RATE.................... 100000000\r\n'
        + 'WORD LEN.................... 16\r\n'
        + 'WORD/FRAME.................. 511\r\n'
        + 'BITS/FRAME.................. 8192\r\n'
        + 'MODULATION TYPE............. PCM/FM\r\n'
        + 'TX Digital Los.............. 0.75\r\n'
        + 'RX Digital Los.............. 0.10\r\n'
        + 'BEP Test.................... -5\r\n'
        + 'Local Rtrn Count............ 00\r\n'
        + 'Remote Rtrn Count........... 00';

        mathMocked.random.mockReturnValue(0);

        await store.dispatch(checkTransmission());

        const {
            telemetry: initialTelemetry,
            ...initialSateRest
        } = initialState;

        const {
            telemetry: finalTelemetry,
            ...finalSateRest
        } = store.getState();

        expect(initialSateRest).toEqual(finalSateRest);
        expect(finalTelemetry).toEqual({
            diagnosticInfo
        });
    })
})