import IMath from "../tirepressuremonitoring/math.interface";
import TelemetryClient from "./telemetryClient";
import TelemetryDiagnosticControls from "./telemetryDiagnosticControls";
import telemetryDiagnosticControls from "./telemetryDiagnosticControls";
import { ITelemetryDiagnosticControlsFactory } from "./telemetryDiagnosticControlsFactory.interface";

export class TelemetryDiagnosticControlsFactory implements ITelemetryDiagnosticControlsFactory {
    private readonly math: IMath;

    constructor(math: IMath) {
        this.math = math;
    }

    create(): telemetryDiagnosticControls {
        const telemetryClient = new TelemetryClient(this.math);
        return new TelemetryDiagnosticControls(telemetryClient);
    }
}