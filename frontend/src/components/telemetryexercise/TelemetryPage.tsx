import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import checkTransmission from '../../telemetry/checkTransmission';

const TelemetryPage = () => {
    const selector = state => state.telemetry.diagnosticInfo;
    
    const dispatch = useDispatch();
    const diagnosticInfo = useSelector(selector);
    const layoutDiagnosticInfo = 
        diagnosticInfo ? 
            <>{diagnosticInfo}</> :
            <>No diagnostic info</>;

    return <>
        <h2>Telemetry Exercise</h2>
        <br />
        <p>write the unit tests for the TelemetryDiagnosticControls class. The responsibility of the TelemetryDiagnosticControls class is to establish a connection to the telemetry server
             (through the TelemetryClient), send a diagnostic request and successfully receive the response that contains the diagnostic info. 
             The TelemetryClient class provided for the exercise fakes the behaviour of the real TelemetryClient class, and can respond with 
             either the diagnostic information or a random sequence. The real TelemetryClient class would connect and communicate with the telemetry server via tcp/ip.</p>
        <br />
        <button onClick={
            () => dispatch(checkTransmission())
        }>Get Diagnostic Info</button>
        <br />
        {layoutDiagnosticInfo}
        <br />
        <br />
        <Link to="/">Back</Link>
    </>;
}

export default TelemetryPage;