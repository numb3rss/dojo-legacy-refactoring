import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import retrieveTurnNumber from '../../turnticketdispenser/ticketDispenser';

const TurnTicketDispenserPage = () => {
    const selector = state => state.turnTicketDispenser.turnNumber;
    
    const dispatch = useDispatch();
    const turnNumber = useSelector(selector);
    const layoutTurnNumber = 
        turnNumber > -1? 
            <div id="turn-number">turnNumber: {turnNumber}</div> :
            <>No turn was given</>;

    return <>
        <h2>Turn Ticket Dispenser Exercise</h2>
        <br />
        <p>write the unit tests for the TicketDispenser. The TicketDispenser class is designed to be used to manage a queuing system in a shop. 
            There may be more than one ticket dispenser but the same ticket should not be issued to two different customers.</p>
        <br />
        <button onClick={
            () => dispatch(retrieveTurnNumber())
        }>Get Turn Number</button>
        <br />
        {layoutTurnNumber}
        <br />
        <br />
        <Link to="/">Back</Link>
    </>;
}

export default TurnTicketDispenserPage;