import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import checkTirePressure from '../../tirepressuremonitoring/checkTirePressure';

const TirePressureMonitoringPage = () => {
    const selector = state => state.tirePressureMonitoring.isTooHigh;
    
    const dispatch = useDispatch();
    const isTooHigh = useSelector(selector);
    const layoutAlarm = isTooHigh ? <>Alarm is on !</> : <>Alarm is off !</>;

    return <>
        <h2>Turn Pressure Monitoring Exercise</h2>
        <br />
        <p>write the unit tests for the Alarm class. The Alarm class is designed to monitor tire pressure and set an alarm
             if the pressure falls outside of the expected range. The Sensor class provided for the exercise fakes the behaviour 
             of a real tire sensor, providing random but realistic values.</p>
        <br />
        <button onClick={
            () => dispatch(checkTirePressure())
        }>Check Tire Pressure</button>
        <br />
        {layoutAlarm}
        <br />
        <br />
        <Link to="/">Back</Link>
    </>;
}

export default TirePressureMonitoringPage;