import React from 'react';
import { Link } from 'react-router-dom';
import { TELEMETRY_EXERCISE_URL, TIRE_PRESSURE_MONITORING_EXERCISE_URL, TURN_TICKET_DISPENSER_EXERCISE_URL } from '../routing/route.config';

const HomePage = () => {
    return <>
            <p>Welcome to legacy refactoring dojo</p>
            <nav>
                <ul>
                    <li><Link to={TELEMETRY_EXERCISE_URL}>Telemetry Exercise</Link></li>
                    <li><Link to={TURN_TICKET_DISPENSER_EXERCISE_URL}>Turn Ticket Dispenser Exercise</Link></li>
                    <li><Link to={TIRE_PRESSURE_MONITORING_EXERCISE_URL}>Tire Pressure Monitoring Exercise</Link></li>
                </ul>
            </nav>
    </>;
}

export default HomePage;
