import { ThunkResult } from '../redux/store/reduxStoreConfiguration';
import { storeTirePressure } from '../redux/tirepressuremonitoring/tirepressuremonitoring.action';

const checkTirePressure = (): ThunkResult<void> => (dispatch, getState, dependencies) => {
    const {
        alarmFactory
    } = dependencies;
    const alarm = alarmFactory.create();

    alarm.check();

    dispatch(storeTirePressure(alarm.isAlarmOn()));
}

export default checkTirePressure;
