export default interface IAlarm {
    check(): void;
    isAlarmOn(): boolean; 
}