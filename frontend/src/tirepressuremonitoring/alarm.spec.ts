import IAlarm from './alarm.interface'
import Alarm from './alarm'
import ISensor from './sensor.interface';


describe('test alarm', () => {
    let alarm: IAlarm;

    const sensorMocked: jest.Mocked<ISensor> = {
        popNextPressurePsiValue: jest.fn()
    };

    beforeEach(() => {
        alarm = new Alarm(sensorMocked);
    })

    afterEach(()=> {
        jest.clearAllMocks();
    })

    it('Alarm should be init with falsy value', () => {
        const alarmOn = alarm.isAlarmOn();

        expect(alarmOn).toBeFalsy();
    });

    it('Alarm should be truthy with inferior value', () => {
        sensorMocked.popNextPressurePsiValue.mockReturnValue(5);

        alarm.check();

        expect(alarm.isAlarmOn()).toBeTruthy();
    });

    it('Alarm should be truthy with superior value', () => {
        sensorMocked.popNextPressurePsiValue.mockReturnValue(30);

        alarm.check();

        expect(alarm.isAlarmOn()).toBeTruthy();
    });

    it('Alarm should be falsy with value between 17 & 21', () => {
        sensorMocked.popNextPressurePsiValue.mockReturnValue(20);

        alarm.check();

        expect(alarm.isAlarmOn()).toBeFalsy();
    });
});