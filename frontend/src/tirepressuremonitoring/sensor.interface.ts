export default interface ISensor {
    popNextPressurePsiValue(): number;
}