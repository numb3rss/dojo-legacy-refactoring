import Sensor from './sensor'
import MathFormula from './math'
import ISensor from './sensor.interface';
import IMath from './math.interface';


describe('test Sensor', () => {
    let sensor: ISensor;

    const mathFormulaMocked: jest.Mocked<IMath> = {
        random: jest.fn()
    };

    beforeEach(() => {
        sensor = new Sensor(mathFormulaMocked);
    })

    afterEach(()=> {
        jest.clearAllMocks();
    })

    it('Sensor pressure should return offset value', () => {
        mathFormulaMocked.random.mockReturnValue(0);

        const psiValue = sensor.popNextPressurePsiValue();

        expect(psiValue).toBe(16);
    });

    it('Sensor pressure should add offset value', () => {
        mathFormulaMocked.random.mockReturnValue(1);

        const psiValue = sensor.popNextPressurePsiValue();

        expect(psiValue).toBe(22);
    });
});