import alarmInterface from "./alarm.interface";
import { IAlarmFactory } from "./alarmFactory.interface";
import Alarm from "./alarm";
import Sensor from "./sensor";
import IMath from "./math.interface";

export class AlarmFactory implements IAlarmFactory {
    private readonly math;

    constructor(math: IMath) {
        this.math = math;
    }

    create(): alarmInterface {
        const sensor = new Sensor(this.math);
        return new Alarm(sensor);
    }
}