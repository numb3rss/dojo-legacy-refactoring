import { configureStore } from '../redux/store/reduxStoreConfiguration';
import IMath from './math.interface';
import { AlarmFactory } from './alarmFactory';
import checkTirePressure from './checkTirePressure';

describe("Acceptance tests for use case checkTirePressure", () => {
    let store;
    let initialState;

    const mathMocked: jest.Mocked<IMath> = {
        random: jest.fn()
    }

    beforeEach(() => {
        const alarmFactory = new AlarmFactory(mathMocked);
        store = configureStore({
            alarmFactory
        });
        initialState = store.getState();
    });

    it('should return alarm on when tire pressure is too low', async () => {
        mathMocked.random.mockReturnValue(0);

        await store.dispatch(checkTirePressure());

        const {
            tirePressureMonitoring: initialTirePressureMonitoring,
            ...initialSateRest
        } = initialState;

        const {
            tirePressureMonitoring: finalTirePressureMonitoring,
            ...finalSateRest
        } = store.getState();

        expect(initialSateRest).toEqual(finalSateRest);
        expect(finalTirePressureMonitoring).toEqual({
            isTooHigh: true
        });
    })

    it('should return alarm on when tire pressure is too high', async () => {
        mathMocked.random.mockReturnValue(1);

        await store.dispatch(checkTirePressure());

        const {
            tirePressureMonitoring: initialTirePressureMonitoring,
            ...initialSateRest
        } = initialState;

        const {
            tirePressureMonitoring: finalTirePressureMonitoring,
            ...finalSateRest
        } = store.getState();

        expect(initialSateRest).toEqual(finalSateRest);
        expect(finalTirePressureMonitoring).toEqual({
            isTooHigh: true
        });
    })

    it('should return alarm off when tire pressure is normal', async () => {
        mathMocked.random.mockReturnValue(0.5);

        await store.dispatch(checkTirePressure());

        const {
            tirePressureMonitoring: initialTirePressureMonitoring,
            ...initialSateRest
        } = initialState;

        const {
            tirePressureMonitoring: finalTirePressureMonitoring,
            ...finalSateRest
        } = store.getState();

        expect(initialSateRest).toEqual(finalSateRest);
        expect(finalTirePressureMonitoring).toEqual({
            isTooHigh: false
        });
    })
})