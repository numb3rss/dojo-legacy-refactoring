// The reading of the pressure value from the sensor is simulated in this implementation.
// Because the focus of the exercise is on the other class.

import ISensor from "./sensor.interface";
import IMath from "./math.interface";

export default class Sensor implements ISensor {
	math: IMath;

	constructor(math: IMath) {
		this.math = math;
	}

	public popNextPressurePsiValue(): number {
		const pressureTelemetryValue = this.samplePressure();
		return this.offset() + pressureTelemetryValue;
	}

	private samplePressure(): number {
		// placeholder implementation that simulate a real sensor in a real tire
		const pressureTelemetryValue = Math.floor(6 * this.math.random() * this.math.random());
		return pressureTelemetryValue;
	}

	private offset() { return 16; }
}
