import IAlarm from './alarm.interface'

export interface IAlarmFactory {
    create(): IAlarm
}