describe('Turn ticket dispenser page end 2 end test', () => {
    beforeEach(() => {
        cy.window(2000);
    })

    it('should get turn number when clicking once', () => {
        cy.visit('http://localhost:3000/turn-ticket-dispenser');

        cy.get('div#root > button').click().click();

        cy.get('div#root > div#turn-number').first().should('have.text', 'turnNumber: 1');
    })
})