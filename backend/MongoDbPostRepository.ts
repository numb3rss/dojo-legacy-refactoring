import { PostRepository } from "./PostRepository";

const Post = require('./Post');

export class MongoDbPostRepository implements PostRepository {
    getPosts = (): Promise<any[]> => {
        return Post.find();
    }
}