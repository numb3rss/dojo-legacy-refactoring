const mongooseTest = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

/**
 * Connect to the in-memory database.
 */
module.exports.connect = async () => {
    const mongod = await MongoMemoryServer.create();
    const uri = await mongod.getUri();

    const mongooseOpts = {
        dbName: 'jest'
    };

    await mongooseTest.connect(uri, mongooseOpts);
}

/**
 * Drop database, close the connection and stop mongod.
 */
module.exports.closeDatabase = async () => {
    await mongooseTest.connection.dropDatabase();
    await mongooseTest.connection.close();
}

/**
 * Remove all the data for all db collections.
 */
module.exports.clearDatabase = async () => {
    const collections = mongooseTest.connection.collections;

    for (const key in collections) {
        const collection = collections[key];
        await collection.deleteMany();
    }
}