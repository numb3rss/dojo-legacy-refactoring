const Post = require('./Post');
const appTest = require("./server");
const supertest = require("supertest");
const dbHandler = require('./dbHandler');

describe('end 2 end tests', () => {
  /**
   * Connect to a new in-memory database before running any tests.
   */
  beforeAll(async () => await dbHandler.connect());

  /**
   * Clear all test data after every test.
   */
  afterEach(async () => await dbHandler.clearDatabase());

  /**
   * Remove and close the db and server.
   */
  afterAll(async () => await dbHandler.closeDatabase());

  test("GET /posts", async () => {
    const post = { title: "Post 1", content: "Lorem ipsum" };
    Post.create(post);

    await supertest(appTest).get("/posts")
      .expect(200)
      .then((response: any) => {
        expect(Array.isArray(response.body)).toBeTruthy();
        expect(response.body.length).toEqual(1);
        expect(response.body[0].title).toBe(post.title);
        expect(response.body[0].content).toBe(post.content);
      });
  });
})