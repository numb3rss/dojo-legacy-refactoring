mongo -- "jest" <<EOF
    use jest;
    db.posts.insertOne({ title: "Post 1", content: "Lorem ipsum" });
    db.createUser({user: 'jest', pwd: 'jest', roles: [{ role: "readWrite", db: "jest" }]});
EOF