import express from 'express';
import HtmlTextConverter from './htmlTextConverter';
import { MongoDbPostRepository } from './MongoDbPostRepository';

const Post = require('./Post');
const path = require('path');
const app = express();

app.get('/', (req, res) => res.send('Express + TypeScript Server'));

app.get('/convert', (req, res) => {
  const filePath = path.resolve(__dirname, "./texttoconvert.txt");
  const htmlTextConverter = new HtmlTextConverter(filePath);
  const convertedText = htmlTextConverter.convertToHtml();
  res.send({
    convertedText
  });
});

app.get("/posts", async (req, res) => {
  const postRepository = new MongoDbPostRepository();
  const posts = await postRepository.getPosts();
  res.send(posts);
});

app.post("/posts", async (req, res) => {
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
  });
  await post.save();
  res.send(post);
});

module.exports = app;