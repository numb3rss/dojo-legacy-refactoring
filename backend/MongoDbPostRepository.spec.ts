import { MongoDbPostRepository } from "./MongoDbPostRepository";

const mongooseTest = require("mongoose");

describe('Integrations tests MongoDbPostRepository', () => {
    beforeEach((done) => {
        mongooseTest.connect("mongodb://jest:jest@localhost:27017/jest",
          { useNewUrlParser: true, useUnifiedTopology: true },
          () => done());
        const db = mongooseTest.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
      });
      
      afterEach((done) => {
        mongooseTest.connection.db.dropDatabase(() => {
          mongooseTest.connection.close(() => done())
        });
      });
      
      test("get posts should give one post", async () => {
        const post = { title: "Post 1", content: "Lorem ipsum" };
        const postRepository = new MongoDbPostRepository();
      
        const posts = await postRepository.getPosts();
      
        expect(posts.length).toEqual(1);
        expect(posts[0].title).toBe(post.title);
        expect(posts[0].content).toBe(post.content);
      });
})
